// Description----------------------------------------------------------------|
/*
 */

// DEFINES AND INCLUDES-------------------------------------------------------|
#define STM32F051
#include "stm32f0xx.h"

// GLOBAL VARIABLES ----------------------------------------------------------|


// FUNCTION DECLARATIONS -----------------------------------------------------|

int main(void);                                                   //COMPULSORY
void ResetClockTo48Mhz(void);									  //COMPULSORY

void init_ADC();

void init_timer_6();		//50ms interrupt

void init_timer_2();		//PWM


// MAIN FUNCTION -------------------------------------------------------------|

int main()
{
	ResetClockTo48Mhz();										   //COMPULSORY

	init_ADC();

	init_timer_2();

	init_timer_6();

	while(1);
}

// OTHER FUNCTIONS -----------------------------------------------------------|
/* Description:
 * This function resets the STM32 Clocks to 48 MHz
 */
 void ResetClockTo48Mhz(void)									   //COMPULSORY
{																   //COMPULSORY
	if ((RCC->CFGR & RCC_CFGR_SWS) == RCC_CFGR_SWS_PLL)			   //COMPULSORY
	{															   //COMPULSORY
		RCC->CFGR &= (uint32_t) (~RCC_CFGR_SW);					   //COMPULSORY
		while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_HSI);	   //COMPULSORY
	}															   //COMPULSORY
	RCC->CR &= (uint32_t)(~RCC_CR_PLLON);						   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) != 0);						   //COMPULSORY
	RCC->CFGR = ((RCC->CFGR & (~0x003C0000)) | 0x00280000);		   //COMPULSORY
	RCC->CR |= RCC_CR_PLLON;									   //COMPULSORY
	while ((RCC->CR & RCC_CR_PLLRDY) == 0);						   //COMPULSORY
	RCC->CFGR |= (uint32_t) (RCC_CFGR_SW_PLL);					   //COMPULSORY
	while ((RCC->CFGR & RCC_CFGR_SWS) != RCC_CFGR_SWS_PLL);		   //COMPULSORY
}

void init_timer_2()		// init PWM with TIM2.
{
	// Enable GPIOB clock							//3 mark for pins setup
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;
	// Setup GPIOB pin 3 as alternate function
	GPIOB->MODER |= GPIO_MODER_MODER3_1;
	// Setup GPIOA pin 2 as alternate function
	GPIOA->MODER |= GPIO_MODER_MODER2_1;
	// Select alternate function: TIM2_CH2
	GPIOB->AFR[0] |= (2 << 3*4);
	// Select alternate function: TIM2_CH3
	GPIOA->AFR[0] |= (2 << 2*4);

	// Enable Timer 3 clock
	RCC->APB1ENR |= RCC_APB1ENR_TIM2EN;							//1
	// Set clock source to 15 kHz
	TIM2->PSC = 2;												//1 - frequency
	// Setup ARR to reload after 4095 (for 12 bit ADC) counts
	TIM2->ARR = 1023;
	// Output, PWM mode 1, enable OC 2 preload
	TIM2->CCMR1 |= TIM_CCMR1_OC2M_2 | TIM_CCMR1_OC2M_1 | TIM_CCMR1_OC2PE;	//2
	// Output, PWM mode 1, enable OC 3 preload
	TIM2->CCMR2 |= TIM_CCMR2_OC3M_2 | TIM_CCMR2_OC3M_1 | TIM_CCMR2_OC3PE;	//2
	// Output  TIM2 OC 2, 3 signal, OC3 active low
	TIM2->CCER |= TIM_CCER_CC2E | TIM_CCER_CC3E | TIM_CCER_CC3P;		//3

	// Enable counter for Timer 3
	TIM2->CR1 |= TIM_CR1_CEN;		//1

	TIM2->CCR2 = 1024/4;			//1
	TIM2->CCR3 = 1024/4;
}

void init_timer_6()
{										//5- clock enable, delay, UIE, enable,
										// NVIC			//50ms interrupt
	RCC->APB1ENR |= RCC_APB1ENR_TIM6EN;

	TIM6->PSC = 1;
	TIM6->ARR = 48000;

	TIM6->DIER |= TIM_DIER_UIE;
	TIM6->CR1 |= TIM_CR1_CEN;

	NVIC_EnableIRQ(TIM6_IRQn);
}


void init_ADC()
{
	//
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	//enable clock for port a
	GPIOA->MODER |= GPIO_MODER_MODER6|GPIO_MODER_MODER5;
	//set pa5 and pa6 to AF
	RCC->APB2ENR |= RCC_APB2ENR_ADCEN;
	//enable clock for ADC
	ADC1->CR |= ADC_CR_ADEN;
	//ADC enable
	ADC1->CHSELR |= ADC_CHSELR_CHSEL5|ADC_CHSELR_CHSEL6;
	// Select channel 5 and 6
	ADC1->CFGR1&=~ADC_CFGR1_SCANDIR;
	//forward scan mode
	ADC1->CFGR1 |= ADC_CFGR1_WAIT;
	// Set up in wait mode
	ADC1->CFGR1 &=~ ADC_CFGR1_RES;					//1
	//12 bit resolution
	while((ADC1->ISR & ADC_ISR_ADRDY) == 0);
}

// INTERRUPT HANDLERS --------------------------------------------------------|

void TIM6_IRQHandler()
{
	TIM6->SR &= ~TIM_SR_UIF;			 		//acknowledge interrupt

	ADC1->CR |= ADC_CR_ADSTART;				//1		//start read
	while((ADC1->ISR & ADC_ISR_EOC) == 0);			//wait for conversion
	uint16_t PA5 = ADC1->DR;
	while((ADC1->ISR & ADC_ISR_EOC) == 0);			//wait for conversion
	uint16_t PA6 = ADC1->DR;

	float diff= PA5-PA6;
	float error = diff*(10*1023/4095);

	if(error<-511.5)
	{
		error = -511.5; //this ensures values less than 511 with just be be 0%
	}
	else if(error>511.5)
	{
		error = 511.5; //to ensure less than 100 %
	}
}

//22
